<?php

namespace App\Providers;

use App\Post;
use Illuminate\Support\ServiceProvider;
use \App\Billing\Stripe;
use App\Tag;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // make sidebar data avilable to all site pages
        view()->composer('layouts.sidebar', function ($view) {
            $archives = Post::archives();
            $tags     = Tag::has('posts')->pluck('name');

            // passing archives and tags to the view
            $view->with(compact('archives', 'tags'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // registering a stripe key
        $this->app->singleton(Stripe::class, function() {
            return new Stripe(config('services.stripe.secret'));
        });
    }
}
