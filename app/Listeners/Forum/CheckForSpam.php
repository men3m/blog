<?php

namespace App\Listeners\Forum;

use App\Events\Forum\ThreadPublished;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CheckForSpam
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ThreadPublished  $event
     * @return void
     */
    public function handle(ThreadPublished $event)
    {
        var_dump('checking for spam.');
    }
}
