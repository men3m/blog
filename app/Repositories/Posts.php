<?php

namespace App\Repositories;

use App\Post;
use App\Repositories\Redis;

class Posts 
{
  protected $redis;

  // dependancy injection
  public function __construct(Redis $redis)
  {
    $this->redis = $redis;
  }

  public function all()
  {
    return Post::all();
  }

  public function latest($month, $year)
  {
    return Post::latest()->filter($month, $year)->get();
  }
}