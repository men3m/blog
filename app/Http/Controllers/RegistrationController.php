<?php

namespace App\Http\Controllers;

use App\User;
use App\Mail\Welcome;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegistrationRequest;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('registration.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegistrationRequest $regRequest)
    {
        // vlaidate the form
        // $this->validate(request(), [
        //     'name'     => 'required',
        //     'email'    => 'required|email',
        //     'password' => 'required|confirmed'
        // ]);
        
        // create and save the user
        // $user = User::create([
        //     'name'     => request('name'),
        //     'email'    => request('email'),
        //     'password' => Hash::make(request('password'))
        // ]);

        // sign them in
        // auth()->login($user);
        
        // send a welcome email to the user
        // \Mail::to($user)->send(new Welcome($user));
        
        // doing all the 3 steps above with the magic of validation as well
        $regRequest->persist();

        session()->flash('message', 'Thanks for registration!');

        // redirect to the home page
        return redirect()->home();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
