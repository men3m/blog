<?php

namespace App;

use App\Model;
use App\Tag;
use Carbon\Carbon;

class Post extends Model
{
    // implementing soft delete trait
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = ['deleted_at'];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function addComment($body)
    {
        $this->comments()->create([
            'body' => $body,
            'user_id' => auth()->user()->id,
        ]);
    }

    public static function archives()
    {
        return static::selectRaw('year(created_at) year, monthname(created_at) month, count(*) published')
            ->groupBy('year', 'month')
            ->orderByRaw('min(created_at) desc')
            ->get()->toArray();
    }

    public function scopeFilter($query, $filters)
    {
        if ($filters && $month = $filters['month']) {
            $query->whereMonth('created_at', Carbon::parse($month)->month);
        }

        if ($filters && $year = $filters['year']) {
            $query->whereYear('created_at', $year);
        }
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
