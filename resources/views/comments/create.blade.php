<!-- Add Comment -->
<div class="card">
    <div class="card-body">
        <form method="POST" action="/posts/{{ $post->id }}/comments" class="form">
            {{ csrf_field() }}
            <div class="form-group">
                <textarea name="body" class="form-control" placeholder="Your comment here" required></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Add Comment</button>
            </div>
        </form>
        @include('layouts.errors')
    </div>
</div>
