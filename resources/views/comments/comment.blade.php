<ul class="list-group">
    @foreach($post->comments as $comment)
        <li class="list-group-item">
            <strong>{{ $comment->user->name }} - {{ $comment->created_at->diffForHumans() }}</strong>
            <hr>
            <article> 
                {{ $comment->body }}
            </article>
        </li>
    @endforeach
</ul>