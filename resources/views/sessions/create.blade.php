@extends('layouts.main')

@section('content')
  <h1>Login</h1>

  <hr>
  <div class="col-md-8 container">

    <form method="POST" action="/login" class="form">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="email">E-mail</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="email">
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name="password">
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Login</button>
      </div>

      @include('layouts.errors')
    </form>
  </div>
@endsection