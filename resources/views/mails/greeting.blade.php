@component('mail::message')
# Introduction

Thanks, {{ $user->name }} for your registeration!

@component('mail::button', ['url' => ''])
Browese
@endcomponent

@component('mail::panel')
Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam, vitae ea minima quidem modi quos assumenda, temporibus quasi culpa dignissimos dolorem ipsum quia, commodi facilis repellat fugiat nobis molestias. Cumque.
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
