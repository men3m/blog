@extends('layouts.main')

@section('content')
  
  <h1>Register</h1>

  <hr>

  <div class="col-md-8 container">
    <form method="POST" action="/register" class="form">
      {{ csrf_field() }}

      <div class="form-group">
        <label for="name">User Name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="User Name" required>
      </div>
      <div class="form-group">
        <label for="email">E-Mail</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="E-Mail" required>
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name="password" required>
      </div>
      <div class="form-group">
        <label for="password_confirmation">Password Confirmation</label>
        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary">Register</button>
      </div>

      @include('layouts.errors')
      
    </form>
  </div>

@endsection