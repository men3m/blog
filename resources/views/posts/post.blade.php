@extends('layouts.main')

@section('content')
    <div class="blog-post">

        <h2 class="blog-post-title">{{ $post->title }}</h2>

        <p class="blog-post-meta">By&colon; {{ $post->user->name }} on {{ $post->created_at->toFormattedDateString() }}</p>

        <p>
            @if(count($post->tags))
                @foreach($post->tags as $tag)
                    <span>
                        <a href="/posts/tags/{{ $tag->name }}">
                            {{ $tag->name }}
                        </a>
                    </span>
                @endforeach
            @endif
        </p>

        <p class="blog-post-body">{{ $post->body }}</p>

    </div><!-- /.blog-post -->

    @auth
        <hr>

        @include('comments.create')
    @endauth

    <hr>

    @include('comments.comment')

    <br>
@endsection
