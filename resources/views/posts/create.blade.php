@extends('layouts.main')

@section('content')
  <h1>Create a post</h1>

  <hr>

  <div class="row">
    <div class="col-md-12">
      <form method="POST" action="/posts" class="form">

        {{ csrf_field() }}

        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" id="title" name="title" placeholder="Title" required>
        </div>

        <div class="form-group">
          <label for="body">Body</label>
          <textarea class="form-control" id="body" name="body" placeholder="Body" required></textarea>
        </div>

        <div class="form-group">
          <button type="submit" class="form-control btn btn-primary">Publish</button>
        </div>

        @include('layouts.errors')

      </form>
    </div>
  </div>
@endsection
