<div class="p-3 mb-3 bg-light rounded">
    <h4 class="font-italic">About</h4>
    <p class="mb-0">Mohamed AbdEl-Monem <em>(Men3m)</em>. Is a systems analyst and software deveoper, who works well to make all dreams to be reality.</p>
</div>

<div class="p-3">
    <h4 class="font-italic">Archives</h4>
    <ol class="list-unstyled mb-0">
        @foreach($archives as $publishes)
            <li><a href="/?month={{ $publishes['month'] }}&year={{ $publishes['year'] }}">
                {{ $publishes['month'] . ', ' . $publishes['year'] . ' (' . $publishes['published'] . ')' }}</a>
            </li>
        @endforeach
    </ol>
</div>

<div class="p-3">
    <h4 class="font-italic">Tags</h4>
    <ol class="list-unstyled mb-0">
        @foreach($tags as $tag)
            <li>
                <a href="/posts/tags/{{ $tag }}">
                    {{ $tag }}
                </a>
            </li>
        @endforeach
    </ol>
</div>

<div class="p-3">
    <h4 class="font-italic">Elsewhere</h4>
    <ol class="list-unstyled">
        <li><a href="#">GitHub</a></li>
        <li><a href="#">Twitter</a></li>
        <li><a href="#">Facebook</a></li>
    </ol>
</div>
