@if($flash = session('message'))
    <div id="flashed-message" class="alert alert-success" role="alert">
        <strong>{{ $flash }}</strong>
    </div>
@endif