<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>Men3m Blog</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <link href="/css/blog.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">    
  </head>

  <body>

    <div class="container">

      @include('layouts.header')

      <div class="nav-scroller py-1 mb-2">
        @include('layouts.nav')
      </div>

      @include('layouts.messages.flashed')
      
      @yield('jumbotron')
    </div>

    <main role="main" class="container">
      <div class="row">
        <div class="col-md-8 blog-main">
          
          <div class="content">
            @yield('content')
          </div>
          
          <div class="pagination">
            @yield('pagination')
          </div>

        </div><!-- /.blog-main -->

        <aside class="col-md-4 blog-sidebar">
          @include('layouts.sidebar')
        </aside><!-- /.blog-sidebar -->

      </div><!-- /.row -->
    </main><!-- /.container -->

    @include('layouts.footer')
  </body>
</html>
