<?php

/*
 * Posts router
 */

// GET /posts
Route::get('/', 'PostController@index')->name('home');

// GET /posts/create
Route::get('/posts/create', 'PostController@create');

// GET /posts/{id}
Route::get('/posts/{post}', 'PostController@show');

// POST /posts
Route::post('/posts', 'PostController@store');

// GET /posts/{id}/edit
Route::get('/posts/{post}/edit', 'PostController@edit');

// PATCH /posts/{id}
Route::patch('/posts/{post}', 'PostController@update');

// DELETE /posts/{id}
Route::delete('/posts/{post}', 'PostController@destroy');

// GET /posts/tags/{tag}
Route::get('/posts/tags/{tag}', 'TagController@index');

// POST /posts/{post}/comments
Route::post('/posts/{post}/comments', 'CommentController@store');

// GET /register
Route::get('/register', 'RegistrationController@create');

// POST /register
Route::post('/register', 'RegistrationController@store');

// GET /login
Route::get('/login', 'SessionController@create');

// POST /login
Route::post('/login', 'SessionController@store');

// GET /logout
Route::get('/logout', 'SessionController@destroy');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
